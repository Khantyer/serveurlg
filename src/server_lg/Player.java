/*
 * Copyright (C) 2015 Khantyer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package server_lg;

import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author Khantyer
 */
public class Player extends Thread {

    private final WebSocket ws;
    private final Lobby lobby;
    private Game game;
    private boolean founder;
    private boolean wolf;
    private boolean dead;
    private boolean spectator;
    private int role;
    private String pseudo;

    Player(WebSocket webSocket, Lobby lobby) {
        System.out.println("Un client rejoint le serveur");
        ws = webSocket;
        this.lobby = lobby;
        this.spectator = true;
        this.wolf = false;
        onStart();
    }

    public boolean isSpectator() {
        return spectator;
    }

    public void setLoup(boolean loup) {
        this.wolf = loup;
    }

    public boolean isLoup() {
        return wolf;
    }

    public int getRole() {
        return role;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public String getPseudo() {
        return pseudo;
    }

    public Game getGame() {
        return game;
    }

    public void setFounder(boolean founder) {
        this.founder = founder;
    }

    public boolean isFounder() {
        return founder;
    }

    public void setGame(Game game) {
        this.game = game;
        game.addSpectator(this);
        JSONObject listePlayer = new JSONObject();
        ArrayList<JSONObject> playerList = new ArrayList<>();
        for (int i = 0; i < game.playerList.size(); i++) {
            JSONObject joueur = new JSONObject();
            joueur.put("Pseudo", game.playerList.get(i).getPseudo());
            joueur.put("Founder", game.playerList.get(i).isFounder());
            playerList.add(joueur);
        }
        listePlayer.put("Type", 1);
        listePlayer.put("ListeJoueur", playerList);
        listePlayer.put("Etat", game.getPart());
        send(listePlayer.toString());
    }

    private void onStart() {
        this.start();
        getGameList();
    }

    @Override
    public void run() {
        while (!isInterrupted()) {
            try {
                String message = ws.receiveData();
                if (message != null) {
                    decodeMessage(message);
                }
            } catch (IOException ex) {
                System.err.println(ex.getMessage());
                interrupt();
                System.out.println("Un client quitte le serveur");
                if (this.game != null) {
                    if (this.spectator) {
                        game.removeSpectator(this);
                    } else {
                        game.removePlayer(this);
                    }
                } else {
                    lobby.clientList.remove(this);
                }
            }
        }
    }

    private void decodeMessage(String message) {
        JSONObject json = new JSONObject(message);
        int type = json.getInt("Type");
        switch (type) {
            case 0:
                getGameList();
                break;

            case 1:
                createNewGame(json);
                break;
                
            case 2:
                joinGame(json);
                break;
                
            case 3:
                registerNewPlayer(json);
                break;

            case 4:
                chatMessage(json);
                break;
        }
    }

    public void send(String message) {
        try {
            ws.send(message);
        } catch (IOException ex) {
            Logger.getLogger(Player.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void registerNewPlayer(JSONObject json) {
        if (game.getPart() == -1) {
            pseudo = json.getString("Pseudo");
            if (!pseudo.isEmpty()) {
                if (usernameAvailable(pseudo)) {
                    this.spectator = false;
                    game.addPlayer(this);
                } else {
                    identificationError(2);
                }
            } else {
                identificationError(1);
            }
        } else {
            identificationError(0);
        }

    }

    private void chatMessage(JSONObject json) {
        String[] keys = {"Message"};
        JSONObject newMessage = new JSONObject(json, keys);
        if (!spectator && ((game.getPart() == -1) || ((game.getPart() == 1) && this.wolf) || (game.getPart() == 3))) {
            newMessage.put("Type", 3);
            newMessage.put("Pseudo", pseudo);
            game.chat.linkedBlockingQueue.add(newMessage.toString());
        }
    }

    private void joinGame(JSONObject json) {
        setGame(lobby.gameList.get(json.getInt("Id")));
    }

    private void getGameList() {
        ArrayList<JSONObject> gameList = new ArrayList<>();
        for (int i = 0; i < lobby.gameList.size(); i++) {
            gameList.add(new JSONObject());
            gameList.get(i).put("Name", lobby.gameList.get(i).getName());
            gameList.get(i).put("NombreJoueur", lobby.gameList.get(i).playerList.size());
            gameList.get(i).put("Slot", lobby.gameList.get(i).getSlot());
            gameList.get(i).put("Etat", lobby.gameList.get(i).getPart());
        }

        JSONObject listePlayer = new JSONObject();
        listePlayer.put("Type", 0);
        listePlayer.put("ListePartie", gameList);
        send(listePlayer.toString());
    }

    private boolean usernameAvailable(String pseudo) {
        for (int i = 0; i < game.playerList.size(); i++) {
            if (game.playerList.get(i).getPseudo().equals(pseudo)) {
                return false;
            }
        }
        return true;
    }

    private void identificationError(int error) {
        JSONObject jsonError = new JSONObject();
        jsonError.put("Type", -1);
        jsonError.put("Erreur", error);
        send(jsonError.toString());
    }

    private void createNewGame(JSONObject json) {
        setGame(lobby.addGame(json.getString("NomPartie"), json.getInt("Slot")));
    }
}
