/*
 * Copyright (C) 2015 Khantyer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package server_lg;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Khantyer
 */
public class Chat extends Thread {

    public LinkedBlockingQueue<String> linkedBlockingQueue = new LinkedBlockingQueue<>(60);
    Game jeu;

    Chat(Game jeu) {
        this.jeu = jeu;
    }

    @Override
    public void run() {
        while (!isInterrupted()) {
            try {
                jeu.send(linkedBlockingQueue.take());
            } catch (InterruptedException ex) {
                Logger.getLogger(Chat.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

}