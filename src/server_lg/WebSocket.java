/*
 * Copyright (C) 2015 Khantyer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package server_lg;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import static javax.xml.bind.DatatypeConverter.printBase64Binary;

/**
 * @author Khantyer
 */
public class WebSocket {

    private final Socket socket;
    private final InputStream input;
    private final OutputStream output;

    private boolean closed = false;

    public static final int NORMAL_CLOSURE = 1000;
    public static final int GOING_AWAY = 1001;
    public static final int PROTOCOL_ERROR = 1002;
    public static final int INCORRECT_DATA = 1003;
    public static final int NO_STATUS_CODE = 1005;
    public static final int ANORMAL_CLOSING = 1006;
    public static final int INCOHERENT_VALUE = 1007;
    public static final int POLICY_VIOLATION = 1008;
    public static final int TO_BIG_MESSAGE = 1009;
    public static final int UNEXPECTED_CONDITION = 1011;

    /**
     *
     * @param socket
     * @throws IOException
     */
    public WebSocket(Socket socket) throws IOException {
        input = socket.getInputStream();
        output = socket.getOutputStream();
        this.socket = socket;
        parseHeader();
    }

    private void pong(byte[] donnee, int taille) throws IOException {
        output.write(0x8A);
        output.write(taille);
        output.write(donnee);
    }

    public void send(String message) throws IOException {
        output.write(0x81);
        
        byte[] mess = message.getBytes("UTF-8");
        
        int length = mess.length;
        
        if (length < 126) {
            output.write(length);
        } else if (length < 65536) {
            output.write(126);
            output.write(length >> 8);
            output.write(length);
        } else if (length < Integer.MAX_VALUE) {
            output.write(127);
            
            int[] tabLength = new int[8];
            
            for (int i = 7; i > 0; i--) {
                tabLength[i] = length;
                length = length >> 8;
            }
            for (int i = 0; i < 8; i++) {
                output.write(tabLength[i]);
            }
        } else {
            close(TO_BIG_MESSAGE, "Too big message");
        }
        
        output.write(mess);
    }

    private void close() throws IOException {
        System.out.println("Un client quitte le WS");
        this.output.close();
        this.input.close();
        this.socket.close();
    }

    public void close(int code, String reason) throws IOException {
        output.write(0x88);
        int length = reason.length() + 2;

        if (length < 126) {
            output.write(length);
        } else if (length < 65536) {
            output.write(126);
            output.write(length >> 8);
            output.write(length);
        }

        output.write(code >> 8);
        output.write(code);

        output.write(reason.getBytes("UTF-8"));
        closed = true;
    }

    private void close(byte[] reason) throws IOException {
        output.write(0x88);

        if (reason.length < 126) {
            output.write(reason.length);
        } else if (reason.length < 65536) {
            output.write(126);
            output.write(reason.length >> 8);
            output.write(reason.length);
        }

        output.write(reason);
        close();
    }

    public String receiveData() throws IOException {
        int octet = input.read();
        int FIN = octet >> 7;
        int opcode = octet & 0x7F;

        octet = input.read();
        if ((octet & 0x80) != 128) {
            close(PROTOCOL_ERROR, "Client send unmasked data");
        }
        int length = octet & 0x7F;

        switch (length) {
            case 127:
                length = 0;
                for (int i = 0; i < 7; i++) {
                    length = length + input.read();
                    length = length << 8;
                }
                length = length + input.read();
                break;

            case 126:
                length = 0;
                length = input.read() << 8;
                length = input.read() + length;
                break;
        }

        int[] mask = new int[4];
        for (int i = 0; i < 4; i++) {
            mask[i] = input.read();
        }
        
        byte[] message = new byte[length];
        for (int i = 0; i < length; i++) {
            message[i] = (byte) (input.read() ^ mask[i % 4]);
        }

        if (opcode == 0x9) {
            pong(message, length);
            return null;
        }

        if (opcode == 0x8) {
            if (closed) {
                close();
            } else {
                close(message);
            }
            return null;
        }

        if (opcode != 0x1) {
            close(INCORRECT_DATA, "Incorrect data");
            return null;
        } else {
            return new String(message, "UTF-8");
        }
    }

    private void parseHeader() throws IOException {

        String host = null, upgrade = null, connection = null, webSocketKey = null, webSocketVersion = null;
        BufferedReader buffer = new BufferedReader(new InputStreamReader(input));

        if (buffer.readLine().contentEquals("GET / HTTP/1.1")) {
            while (buffer.ready()) {
                String ligne = buffer.readLine();
                if (ligne.startsWith("Host:")) {
                    host = ligne.substring(6);
                }
                if (ligne.startsWith("Upgrade:")) {
                    upgrade = ligne.substring(9);
                }
                if (ligne.startsWith("Connection:")) {
                    connection = ligne.substring(12);
                }
                if (ligne.startsWith("Sec-WebSocket-Key:")) {
                    webSocketKey = ligne.substring(19);
                }
                if (ligne.startsWith("Sec-WebSocket-Version:")) {
                    webSocketVersion = ligne.substring(23);
                }
            }
            connectionAccept(webSocketKey);
        } else {
            connectionReject();
        }
    }

    private void connectionReject() throws IOException {
        String reponse = "HTTP/1.1 400 Bad Request\r\n";
        output.write(reponse.getBytes());
        close();
    }

    private void connectionAccept(String key) throws IOException {
        key = key + "258EAFA5-E914-47DA-95CA-C5AB0DC85B11";
        MessageDigest digester;
        try {
            digester = MessageDigest.getInstance("SHA-1");
            digester.update(key.getBytes());
            key = printBase64Binary(digester.digest());
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Server_LG.class.getName()).log(Level.SEVERE, null, ex);
        }

        String reponse = "HTTP/1.1 101 Switching Protocols\r\n"
                + "Upgrade: websocket\r\n"
                + "Connection: Upgrade\r\n"
                + "Sec-WebSocket-Accept: " + key + "\r\n"
                + "\r\n";

        output.write(reponse.getBytes());
    }
}
