/*
 * Copyright (C) 2015 david
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package server_lg;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author david
 */
public class Lobby extends Thread {

    public ArrayList<Player> clientList = new ArrayList<>();
    public ArrayList<Game> gameList = new ArrayList<>();
    
    @Override
    public void run() {
        try {
            System.out.println("Creation du WebSocket sur le port 8000");
            ServerSocket serveur = new ServerSocket(8000);
            while (!isInterrupted()) {
                Player newPlayer = new Player(new WebSocket(serveur.accept()), this);
                newPlayer.setName("Player" + getId());
                clientList.add(newPlayer);
            }
        } catch (IOException ex) {
            Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
            interrupt();
        }
    }
    
    public Game addGame(String nom, int slot) {
        Game newGame = new Game(nom, slot, this);
        gameList.add(newGame);
        return newGame;
    }

}
