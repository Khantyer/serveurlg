/*
 * Copyright (C) 2015 Khantyer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package server_lg;

/**
 *
 * @author Khantyer
 */
public class Server_LG {

    
    
    /*
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        System.out.println("Demarrage du serveur");
        
        Lobby lobby = new Lobby();
        lobby.setName("Thread-Lobby");
        lobby.start();
        
        while (lobby.isAlive()) {}
    }
}
