/* 
 * Copyright (C) 2015 Khantyer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package server_lg;

import java.util.ArrayList;
import org.json.JSONObject;

/**
 *
 * @author Khantyer
 */
public class Game extends Thread {

    private final Lobby lobby;
    private int slot;
    private int part;
    public Chat chat;

    private ArrayList<Player> spectatorList = new ArrayList<>();
    public ArrayList<Player> playerList = new ArrayList<>();

    Game(String name, int slot, Lobby lobby) {
        this.setName(name);
        this.slot = slot;
        this.lobby = lobby;
        chat = new Chat(this);
        chat.setName("Thread-Chat-" + getName());
        part = -1;
        onStart();
    }

    public void addPlayer(Player player) {
        JSONObject newPlayer = new JSONObject();
        newPlayer.put("Type", 2);
        newPlayer.put("Pseudo", player.getPseudo());
        send(newPlayer.toString());

        spectatorList.remove(player);
        playerList.add(player);

        if (playerList.size() == 1) {
            newFounder(player);
        }
    }

    @Override
    public void run() {
        part = -1;
    }

    public void removePlayer(Player player) {
        playerList.remove(player);
        JSONObject removePlayer = new JSONObject();
        removePlayer.put("Type", 4);
        removePlayer.put("Pseudo", player.getPseudo());
        send(removePlayer.toString());

        if (player.isFounder() && (!playerList.isEmpty())) {
            newFounder(playerList.get(0));
        }

        if (playerList.isEmpty() && spectatorList.isEmpty()) {
            interrupt();
            chat.interrupt();
            lobby.gameList.remove(this);
        }
    }

    public void addSpectator(Player player) {
        spectatorList.add(player);
    }

    public void removeSpectator(Player player) {
        spectatorList.remove(player);

        if (playerList.isEmpty() && spectatorList.isEmpty()) {
            interrupt();
            chat.interrupt();
            lobby.gameList.remove(this);
        }
    }

    public void newFounder(Player founder) {
        System.out.println("Un nouveau fondateur a été choisi");
        founder.setFounder(true);
        JSONObject message = new JSONObject();
        message.put("Type", 5);
        message.put("Pseudo", founder.getPseudo());
        message.put("Founder", true);
        send(message.toString());
    }

    public void send(String message) {
        for (int i = 0; i < spectatorList.size(); i++) {
            spectatorList.get(i).send(message);
        }
        for (int i = 0; i < playerList.size(); i++) {
            playerList.get(i).send(message);
        }
    }

    public int getPart() {
        return part;
    }

    private void onStart() {
        chat.start();
    }

    public int getSlot() {
        return slot;
    }

    public void setSlot(int slot) {
        this.slot = slot;
    }
}
